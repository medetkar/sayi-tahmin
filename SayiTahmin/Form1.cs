﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SayiTahmin
{
    public partial class Form1 : Form
    {
        int sayi;
        byte hak = 10 , tahminSayisi;
        Random rnd = new Random();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnDene_Click(object sender, EventArgs e)
        {
            hak--;
            int tahmin = Convert.ToInt32(txtSayi.Text);
            if (tahmin == sayi)
            {
                MessageBox.Show("Tebrikler " + (10 - hak).ToString() + " tahminde bildiniz");
                Application.Restart();

            }
            else if (sayi < tahmin)
            {
                lblSonuc.Text = tahmin.ToString() + " sayısından küçük sayı giriniz";
            }
            else if (sayi > tahmin)
            {
                lblSonuc.Text = tahmin.ToString() + " sayısından büyük sayı giriniz";
            }
            btnDene.Text = "Dene\n Kalan Hak Sayısı " + hak.ToString();
            if (hak==0)
            {
                MessageBox.Show("Kaybettiniz tutulan " + sayi.ToString() + " sayısını bilemediniz! ");
                Application.Restart();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            sayi = rnd.Next(0, 100);
            lblTest.Text = sayi.ToString();
            btnDene.Text = "Dene\n Kalan Hak Sayısı " + hak.ToString();
        }
    }
}
